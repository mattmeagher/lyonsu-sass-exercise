$(document).ready(function(){
    $('.mobile-menu').on('click', function(){
        $('nav').toggleClass('open');
        $('body').toggleClass('slide');
        $(this).toggleClass('hide');
    });
    $('.mobile-close').on('click', function(){
        $('nav').toggleClass('open');
        $('body').toggleClass('slide');
        $('.mobile-menu').toggleClass('hide');
    });
    $('.col').on('click', function(){
        $(this).toggleClass('expanded');
    });
});
